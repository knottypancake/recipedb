# Pasta Primavera

## Ingredients

### Vegetable mix

* 1 Tbsp butter
* 1 Tbsp olive oil
* 2 carrots
* 1 bell pepper
* 1 red onion
* 8 mushrooms
* Garlic

### Pasta

* 1 cup (dry) penne (or whatever you want)

### Serving

* Grated or shredded Parmesan cheese (optional)

## Instructions

This can be made with different mixes of vegetables and types of pasta. Choose your own adventure.

1. Cut all vegetables in relatively even pieces. Chop the garlic.

2. In a pan, melt butter and olive oil together. Sauté the vegetables, hardest first. If the pan gets too dry, add some vegetable or chicken (warning: no longer vegan) stock.

3. Cook the pasta according to the directions on the package.

4. Toss the cooked vegetables and pasta together and serve with freshly ground pepper and Parmesan cheese if desired (warning: no longer vegan).

## Tags

* vegan
* entrée
