# Bleu Cheese Dressing

## Ingredients

* 1 cup buttermilk
* 2 cups mayonnaise (not sweet)
* 1 tsp (heaping) garlic powder
* 1 tsp white pepper
* 6-8 oz bleu cheese

## Instructions

1. Mix and refrigerate, preferably overnight

## Yield

About 3 cups

## Source

Family recipe

## Tags

* dressing
