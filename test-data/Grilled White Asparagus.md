# White Asparagus (Grilled)

## Ingredients

* white asparagus
* olive oil
* salt
* pepper

## Instructions

1. Rinse the white asparagus. Using a knife, remove the bottom ½" of each spear.

2. Remove the tough peel from each spear using a vegetable peeler. Starting just below the tip of each spear, use the peeler to remove the tough covering, turning the spear as you go. Holding the tip with three fingers helps protect the tip while keeping the spear stable for peeling.

3. Brush each spear with olive oil. Sprinkle with salt and pepper.

4. Place the spears on a grill rack over medium-low heat, taking care not to break any of the spears. Turn frequently, every minute or so, using tongs to prevent burning. Grilling uncovered is best due to the constant attention needed during cooking. Spears are ready to serve when there are light brown grill marks on each side. Cooking time will vary according to the specific temperature of the grill being used and the level of doneness desired.

## Tags

* vegan
* side dish
