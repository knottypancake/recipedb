# Glass of Milk

## Ingredients

* 1 cup milk

## Yield

1 serving

## Source

http://example.com/recipes/milk

## Tags

* dairy
* beverage
