# Spicy Peach Salsa

## Ingredients

* 2 cups chopped, peeled fresh peaches
* ¼ cup chopped sweet onion
* 3 Tbsp lime juice
* 3 Tbsp finely chopped, seeded, fresh jalapeño pepper
* 1 clove garlic, minced
* 2 Tbsp snipped fresh cilantro
* ½ tsp sugar

## Instructions

1. Dip peaches into boiling water and then peel the skins, then cut to size.

2. In a medium mixing bowl stir together peaches, onion, lime juice, jalapeño, garlic, cilantro, and sugar.

3. Cover and chill for 1 to 2 hours. Makes 2 cups.

## Tags

* vegan
