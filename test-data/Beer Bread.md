# Beer Bread

## Ingredients

* 1 can beer
* 3 cups flour
* 3 tsp baking powder
* ½ stick melted butter
* 1 tsp salt

## Instructions

1. Mix all the ingredients together in a large mixing bowl.

2. Put the dough into a bread pan and pour the butter over the top.

3. Bake in the oven at 375°F (191°C) for an hour.

## Tags

* bread