package api

import (
	"bytes"
	"database/sql"
	"net/http"
	"path"
	"sort"
	"text/template"

	"codeberg.org/knottypancake/recipedb/internal/infra"
	"codeberg.org/knottypancake/recipedb/internal/transform"
	"codeberg.org/knottypancake/recipedb/internal/util/slice"
	"github.com/gin-gonic/gin"
)

// RecipeSearchForm is a struct for binding the parameters used to search for
// recipes
type RecipeSearchForm struct {
	Name       string   `form:"n" binding:"omitempty,min=3,max=100"`
	Ingredient string   `form:"i" binding:"omitempty,min=3,max=100"`
	Tags       []string `form:"t" binding:"max=100"`
}

// RecipeForm is a struct for binding the recipe name parameter used to fetch
// a recipe
type RecipeForm struct {
	Name string `uri:"name" binding:"required,min=3,max=200"`
}

// GetIndex is the controller for the base (/) path
func GetIndex(ctx *gin.Context, appPath string) {
	ctx.HTML(http.StatusOK, "index.tmpl", gin.H{
		"appPath": appPath,
	})
}

// GetRecipes is the controller for recipe searches
func GetRecipes(ctx *gin.Context, db *sql.DB, appPath string) {
	var params RecipeSearchForm
	err := ctx.ShouldBindQuery(&params)
	if err != nil {
		ctx.HTML(http.StatusBadRequest, "error.tmpl", gin.H{
			"err": err.Error(),
		})
		return
	}

	searchParams := infra.RecipeSearchParams{
		Name:       params.Name,
		Ingredient: params.Ingredient,
		Tags:       params.Tags,
	}

	recipes, err := infra.GetRecipes(ctx, db, searchParams)
	if err != nil {
		ctx.HTML(http.StatusInternalServerError, "error.tmpl", gin.H{
			"err": err.Error(),
		})
		return
	}

	numTags := len(searchParams.Tags)
	searchStrings := make([]string, numTags+2)
	searchStrings[0] = searchParams.Name
	searchStrings[1] = searchParams.Ingredient
	if numTags > 0 {
		copy(searchStrings[2:], searchParams.Tags)
	}
	searchStrings = slice.Filter(searchStrings, func(s string) bool {
		return s != ""
	})

	sort.StringSlice(recipes).Sort()
	ctx.HTML(http.StatusOK, "search.tmpl", gin.H{
		"appPath":       appPath,
		"recipes":       recipes,
		"searchStrings": searchStrings,
	})
}

// GetRecipe is the controller for getting a recipe
func GetRecipe(
	ctx *gin.Context,
	db *sql.DB,
	appPath string,
	recipeDirName string,
	templateDirName string,
) {
	var params RecipeForm
	err := ctx.ShouldBindUri(&params)
	if err != nil {
		ctx.HTML(http.StatusBadRequest, "error.tmpl", gin.H{
			"err": err.Error(),
		})
		return
	}

	fileName, isParsable, err := infra.GetRecipeFileName(ctx, db, params.Name)
	if err != nil {
		ctx.HTML(http.StatusInternalServerError, "error.tmpl", gin.H{
			"err": err.Error(),
		})
		return
	}

	if !isParsable {
		ctx.File(path.Join(recipeDirName, fileName))
		return
	}

	fileContents, err := infra.GetFileContents(recipeDirName, fileName)
	if err != nil {
		ctx.HTML(http.StatusInternalServerError, "error.tmpl", gin.H{
			"err": err.Error(),
		})
		return
	}

	templateContents, err := infra.GetFileContents(templateDirName, "recipe.tmpl")
	if err != nil {
		ctx.HTML(http.StatusInternalServerError, "error.tmpl", gin.H{
			"err": err.Error(),
		})
		return
	}

	tmpl, err := template.New("recipe").Parse(string(templateContents))
	if err != nil {
		ctx.HTML(http.StatusInternalServerError, "error.tmpl", gin.H{
			"err": err.Error(),
		})
		return
	}

	var b bytes.Buffer
	renderedRecipe := transform.Transform(fileContents)
	templateData := struct {
		AppPath string
		Name    string
		Recipe  string
	}{
		AppPath: appPath,
		Name:    params.Name,
		Recipe:  string(renderedRecipe),
	}
	tmpl.Execute(&b, templateData)

	ctx.Data(http.StatusOK, "text/html", b.Bytes())
}

// GetForm is the controller for getting the search form
func GetForm(ctx *gin.Context, db *sql.DB, appPath string) {
	tags, err := infra.GetTags(ctx, db)
	if err != nil {
		ctx.HTML(http.StatusInternalServerError, "error.tmpl", gin.H{
			"err": err.Error(),
		})
		return
	}

	sort.StringSlice(tags).Sort()
	ctx.HTML(http.StatusOK, "form.tmpl", gin.H{
		"appPath": appPath,
		"tags":    tags,
	})
}

// GetOtherFiles is the controller for getting the non-parsable files
func GetOtherFiles(ctx *gin.Context, db *sql.DB, appPath string) {
	others, err := infra.GetUnparsableFiles(ctx, db)
	if err != nil {
		ctx.HTML(http.StatusInternalServerError, "error.tmpl", gin.H{
			"err": err.Error(),
		})
		return
	}

	sort.StringSlice(others).Sort()
	ctx.HTML(http.StatusOK, "others.tmpl", gin.H{
		"appPath": appPath,
		"others":  others,
	})
}
