package model

// Recipe defines the properties of a recipe that are parsed and indexed
type Recipe struct {
	FileName    string
	RecipeName  string
	IsParsable  bool
	Ingredients []string
	Tags        []string
}
