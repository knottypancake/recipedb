package scan

import (
	"regexp"
	"strings"

	"codeberg.org/knottypancake/recipedb/internal/model"
	md "github.com/russross/blackfriday/v2"
)

type ParserState uint

// The set of states used to control parsing
const (
	Begin ParserState = iota
	TitleFound
	ExtractingIngredients
	ExtractingTags
	Invalid
	End
)

// ParserProgress is used to track the state of the parsing process
type ParserProgress struct {
	State            ParserState
	TagsFound        bool
	IngredientsFound bool
}

// ExtractionVisitor is called for each element of the AST. It extracts the
// data that it needs into `recipe` and maintains its state with `progress`
func ExtractionVisitor(
	node *md.Node,
	entering bool,
	progress *ParserProgress,
	recipe *model.Recipe,
) md.WalkStatus {
	if progress.State == Begin {
		if entering && isHeader(node, 1) {
			headText := getHeaderChildText(node)
			if headText != "" {
				progress.State = TitleFound
				recipe.RecipeName = headText

				return md.SkipChildren
			}
		}
	}

	ingRegex := regexp.MustCompile(`(?i)\s*ingredients\s*`)
	tagsRegex := regexp.MustCompile(`(?i)\s*tags\s*`)

	if progress.State == TitleFound {
		if entering && isHeader(node, 2) {
			headText := getHeaderChildText(node)
			if ingRegex.Match([]byte(headText)) {
				progress.State = ExtractingIngredients

				return md.GoToNext
			}
			if tagsRegex.Match([]byte(headText)) {
				progress.State = ExtractingTags

				return md.GoToNext
			}
		}
	}

	if progress.State == ExtractingIngredients {
		if entering && isHeader(node, 2) {
			headText := getHeaderChildText(node)
			if tagsRegex.Match([]byte(headText)) {
				progress.State = ExtractingTags

				return md.GoToNext
			}

			progress.State = TitleFound

			return md.GoToNext
		}
		if entering && node.Type == md.Item {
			ing := getItemText(node)
			recipe.Ingredients = append(recipe.Ingredients, ing)

			return md.SkipChildren
		}
	}

	if progress.State == ExtractingTags {
		if entering && isHeader(node, 2) {
			headText := getHeaderChildText(node)
			if ingRegex.Match([]byte(headText)) {
				progress.State = ExtractingIngredients

				return md.GoToNext
			}

			progress.State = TitleFound

			return md.GoToNext
		}
		if entering && node.Type == md.Item {
			tag := getItemText(node)
			recipe.Tags = append(recipe.Tags, tag)

			return md.SkipChildren
		}
	}

	return md.GoToNext
}

func isHeader(node *md.Node, level int) bool {
	return node.Type == md.Heading && node.HeadingData.Level == level
}

func getHeaderChildText(node *md.Node) string {
	if node.FirstChild != nil {
		return strings.TrimSpace(string(node.FirstChild.Literal))
	}

	return ""
}

func getItemText(itemNode *md.Node) string {
	if itemNode.FirstChild != nil && itemNode.FirstChild.FirstChild != nil {
		return strings.TrimSpace(string(itemNode.FirstChild.FirstChild.Literal))
	}

	return ""
}
