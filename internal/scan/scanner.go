package scan

import (
	"strings"

	"codeberg.org/knottypancake/recipedb/internal/infra"
	"codeberg.org/knottypancake/recipedb/internal/model"
	md "github.com/russross/blackfriday/v2"
)

// ScannerChannels defines the channels used to communicate between the
// goroutines that scan and write the recipe data
type ScannerChannels struct {
	Recipes chan model.Recipe
	Done    chan int
}

// DefaultScannerChannels creates a ScannerChannels instance with the default
// configuration
func DefaultScannerChannels() ScannerChannels {
	sc := ScannerChannels{
		Recipes: make(chan model.Recipe, 10),
		Done:    make(chan int),
	}

	return sc
}

// ScanFiles scan the given files
func ScanFiles(dirName string, files []string, channels ScannerChannels) {
	for _, file := range files {
		ScanFile(dirName, file, channels)
	}
}

// ScanFile scans the given file and puts the result onto the recipe channel
func ScanFile(dirName string, fileName string, channels ScannerChannels) {
	if !strings.HasSuffix(fileName, ".md") {
		recipe := model.Recipe{
			FileName:   fileName,
			RecipeName: fileName,
			IsParsable: false,
		}

		channels.Recipes <- recipe

		return
	}

	fileContents, err := infra.GetFileContents(dirName, fileName)
	if err != nil {
		return
	}

	parser := md.New()
	rootNode := parser.Parse(fileContents)
	progress := ParserProgress{}
	recipe := model.Recipe{FileName: fileName, IsParsable: true}

	rootNode.Walk(
		func(node *md.Node, entering bool) md.WalkStatus {
			return ExtractionVisitor(node, entering, &progress, &recipe)
		},
	)

	channels.Recipes <- recipe
}
