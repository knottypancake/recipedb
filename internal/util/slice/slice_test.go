package slice

import (
	"fmt"
	"testing"
)

// Equal reports whether two slices are equal: the same length and all
// elements equal. If the lengths are different, Equal returns false.
// Otherwise, the elements are compared in increasing index order, and the
// comparison stops at the first unequal pair.
// Floating point NaNs are not considered equal.
// Copied from golang.org/x/exp/slices
func Equal[E comparable](s1, s2 []E) bool {
	if len(s1) != len(s2) {
		return false
	}
	for i := range s1 {
		if s1[i] != s2[i] {
			return false
		}
	}
	return true
}

func TestMapEmpty(t *testing.T) {
	var input []string
	var expected []string
	actual := Map(input, func(s string) string {
		return s
	})
	if !Equal(expected, actual) {
		t.Errorf("Expected %v but got %v", expected, actual)
	}
}

func TestMapSameType(t *testing.T) {
	input := []int{1, 2, 3}
	expected := []int{2, 4, 6}
	actual := Map(input, func(i int) int {
		return i * 2
	})
	if !Equal(expected, actual) {
		t.Errorf("Expected %v but got %v", expected, actual)
	}
}

func TestMapDifferentTypes(t *testing.T) {
	input := []int{7, 11, 13}
	expected := []string{"7", "11", "13"}
	actual := Map(input, func(i int) string {
		return fmt.Sprintf("%d", i)
	})
	if !Equal(expected, actual) {
		t.Errorf("Expected %v but got %v", expected, actual)
	}
}

func TestUniqEmpty(t *testing.T) {
	var input []string
	var expected []string
	actual := Uniq(input)
	if !Equal(expected, actual) {
		t.Errorf("Expected %v but got %v", expected, actual)
	}
}

func TestUniqAlreadyUnique(t *testing.T) {
	input := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	expected := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	actual := Uniq(input)
	if !Equal(expected, actual) {
		t.Errorf("Expected %v but got %v", expected, actual)
	}
}

func TestUniqStrings(t *testing.T) {
	input := []string{"red", "blue", "green", "blue", "red"}
	expected := []string{"red", "blue", "green"}
	actual := Uniq(input)
	if !Equal(expected, actual) {
		t.Errorf("Expected %v but got %v", expected, actual)
	}
}
