package slice

// Map applies a function f to all elements of slice s and returned a new slice
// with the result.
func Map[T1, T2 any](s []T1, f func(T1) T2) []T2 {
	r := make([]T2, len(s))
	for i, v := range s {
		r[i] = f(v)
	}

	return r
}

// Filter filters values from a slice using a given filter function. It returns
// a new slice with only the elements of s for which f() returned true.
func Filter[T any](s []T, f func(T) bool) []T {
	var r []T
	for _, v := range s {
		if f(v) {
			r = append(r, v)
		}
	}

	return r
}

// Uniq removes duplicate values from a slice of any comparable type. The given
// slice is unmodified; a new slice is returned. This is a slow and simple
// implementation and should not be used on large slices.
func Uniq[T comparable](s []T) []T {
	r := make([]T, 0, len(s))

outer:
	for _, v := range s {
		for _, w := range r {
			if v == w {
				continue outer
			}
		}

		r = append(r, v)
	}

	return r
}
