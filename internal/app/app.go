package app

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"net"
	"net/http"
	"path"
	"strings"
	"sync"
	"time"

	"codeberg.org/knottypancake/recipedb/internal/api"
	"codeberg.org/knottypancake/recipedb/internal/infra"
	"codeberg.org/knottypancake/recipedb/internal/scan"
	"github.com/fsnotify/fsnotify"
	"github.com/gin-gonic/gin"
	_ "github.com/mattn/go-sqlite3"
)

// AppConfig defines the configuration options for the application
type AppConfig struct {
	Port               string
	AppPath            string
	DatabaseDirName    string
	RecipeDirName      string
	StaticAssetDirName string
	TemplateDir        string
}

// Run scaffolds the app and starts the API listener
func Run(ctx context.Context, config AppConfig) error {
	// Initialize the DB
	dbFileName := path.Join(config.DatabaseDirName, "recipes.db")
	dsn := fmt.Sprintf("file:%s?mode=rwc", dbFileName)
	db, err := sql.Open("sqlite3", dsn)
	if err != nil {
		return fmt.Errorf("database file '%s' could not be opened: %w", dbFileName, err)
	}
	defer func() {
		log.Println("closing database")
		db.Close()
	}()

	err = infra.InitDatabase(ctx, db)
	if err != nil {
		return fmt.Errorf("could not initialize database: %w", err)
	}

	// Start the recipe writer goroutine
	channels := scan.DefaultScannerChannels()
	defer func() {
		log.Println("closing recipe scanner channel")
		close(channels.Done)
	}()
	go func() {
		waitForRecipes(ctx, db, channels)
	}()

	// Scan the recipe dir and get a list of files
	if !infra.FileExists(config.RecipeDirName) {
		return fmt.Errorf("the specified recipe directory '%s' does not exist", config.RecipeDirName)
	}
	recipeFiles, err := infra.FindRecipeFiles(config.RecipeDirName, recipeFilenameIsValid)
	if err != nil {
		return fmt.Errorf("could not find recipe files: %w", err)
	}

	// Start the scanner goroutines and then wait for them to finish
	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		defer wg.Done()
		scan.ScanFiles(config.RecipeDirName, recipeFiles[:len(recipeFiles)/2], channels)
	}()
	go func() {
		defer wg.Done()
		scan.ScanFiles(config.RecipeDirName, recipeFiles[len(recipeFiles)/2:], channels)
	}()

	wg.Wait()

	// Set a watcher on the recipe directory
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return fmt.Errorf("could not create watcher: %w", err)
	}
	defer func() {
		log.Println("closing recipe directory watcher")
		watcher.Close()
	}()
	go func() {
		waitForRecipeChanges(ctx, db, watcher, channels, recipeFilenameIsValid)
	}()
	err = watcher.Add(config.RecipeDirName)
	if err != nil {
		return fmt.Errorf("could not watch recipe directory: %w", err)
	}

	// Start the HTTP server in a goroutine so that it won't block the
	// graceful shutdown handling below
	router := setupRouter(config, db)
	srv := &http.Server{
		Addr:    ":" + config.Port,
		Handler: router,
		BaseContext: func(_ net.Listener) context.Context {
			return ctx
		},
	}

	go func() {
		srv.ListenAndServe()
	}()

	// Listen for the interrupt signal so we can shutdown gracefully
	<-ctx.Done()

	return nil
}

// recipeFilenameIsValid provides logic for determining whether a file should
// be included based on its file name. This logic is used in several places, so
// it is defined here and then passed to other places that need it
func recipeFilenameIsValid(fileName string) bool {
	fileName = strings.TrimSpace(fileName)

	if strings.HasPrefix(fileName, ".") {
		return false
	}

	if !strings.HasSuffix(fileName, ".md") &&
		!strings.HasSuffix(fileName, ".pdf") &&
		!strings.HasSuffix(fileName, ".txt") {
		return false
	}

	return true
}

// waitForRecipes is used by the DB writer goroutine to handle recipes on the
// Recipes channel
func waitForRecipes(ctx context.Context, db *sql.DB, channels scan.ScannerChannels) {
	for {
		select {
		case r := <-channels.Recipes:
			infra.InsertRecipe(ctx, db, r)
		case <-channels.Done:
			return
		}
	}
}

// waitForRecipeChanges is used by the watcher goroutine to handle changes to
// recipe files
func waitForRecipeChanges(
	ctx context.Context,
	db *sql.DB,
	watcher *fsnotify.Watcher,
	channels scan.ScannerChannels,
	isValid func(string) bool,
) {
	// Create a map of timers (one per file) to de-duplicate write events
	var (
		waitFor = 100 * time.Millisecond
		timers  = make(map[string]*time.Timer)
	)

	for {
		select {
		case err, ok := <-watcher.Errors:
			if !ok {
				return
			}
			log.Println("watcher error:", err)

		case event, ok := <-watcher.Events:
			if !ok {
				return
			}

			if !isValid(path.Base(event.Name)) {
				continue
			}

			if event.Has(fsnotify.Remove) || event.Has(fsnotify.Rename) {
				infra.RemoveRecipe(ctx, db, path.Base(event.Name))
				continue
			}

			if !event.Has(fsnotify.Create) && !event.Has(fsnotify.Write) {
				continue
			}

			t, ok := timers[event.Name]
			if !ok {
				t = time.AfterFunc(waitFor, func() {
					updateRecipe(db, event, channels)
					delete(timers, event.Name)
				})
				timers[event.Name] = t
				continue
			}
			t.Reset(waitFor)
		}
	}
}

// updateRecipe is called when a recipe file has been updated and needs to be
// (re-)scanned
func updateRecipe(db *sql.DB, event fsnotify.Event, channels scan.ScannerChannels) {
	dirName := path.Dir(event.Name)
	fileName := path.Base(event.Name)

	if event.Has(fsnotify.Create) || event.Has(fsnotify.Write) {
		scan.ScanFile(dirName, fileName, channels)
	}
}

// setupRouter sets up the API routing
func setupRouter(config AppConfig, db *sql.DB) *gin.Engine {
	r := gin.Default()
	r.SetTrustedProxies(nil)
	r.LoadHTMLGlob(path.Join(config.TemplateDir, "*.tmpl"))

	// index page with unfiltered results (full HTML page)
	r.GET("/", func(ctx *gin.Context) {
		api.GetIndex(ctx, config.AppPath)
	})

	// execute a search (HTML fragment)
	r.GET("/recipes", func(ctx *gin.Context) {
		api.GetRecipes(ctx, db, config.AppPath)
	})

	// the search form
	r.GET("/recipes/form", func(ctx *gin.Context) {
		api.GetForm(ctx, db, config.AppPath)
	})

	// a list of other (non-markdown) files (HTML fragment)
	r.GET("/recipes/other", func(ctx *gin.Context) {
		api.GetOtherFiles(ctx, db, config.AppPath)
	})

	// one recipe for display (full HTML page)
	r.GET("/recipes/:name", func(ctx *gin.Context) {
		api.GetRecipe(ctx, db, config.AppPath, config.RecipeDirName, config.TemplateDir)
	})

	r.Static("/assets", config.StaticAssetDirName)
	r.StaticFile("/favicon.ico", path.Join(config.StaticAssetDirName, "img", "favicon.ico"))

	return r
}
