package infra

import (
	"errors"
	"os"
	"path"
)

// FindRecipeFiles scans a given directory and returns a slice of file names
// that look like valid recipe files
func FindRecipeFiles(dirName string, isValid func(string) bool) ([]string, error) {
	entries, err := os.ReadDir(dirName)
	if err != nil {
		return nil, err
	}

	recipeFiles := make([]string, 0, len(entries))
	for _, entry := range entries {
		if entry.IsDir() || !isValid(entry.Name()) {
			continue
		}

		recipeFiles = append(recipeFiles, entry.Name())
	}

	return recipeFiles, nil
}

// GetFileContents is a thin abstraction over os.ReadFile()
func GetFileContents(dirName string, fileName string) ([]byte, error) {
	return os.ReadFile(path.Join(dirName, fileName))
}

// FileExists returns whether the given filename exists
func FileExists(filename string) bool {
	_, error := os.Stat(filename)

	return !errors.Is(error, os.ErrNotExist)
}
