package infra

import (
	"context"
	"database/sql"
	"strings"

	"codeberg.org/knottypancake/recipedb/internal/util/slice"
	"github.com/jmoiron/sqlx"
)

// RecipeSearchParams defines the parameters used to search for recipes
type RecipeSearchParams struct {
	Name       string
	Ingredient string
	Tags       []string
}

// GetRecipes searches for recipes with the given parameters and returns a
// slice of matching recipe names
func GetRecipes(ctx context.Context, db *sql.DB, params RecipeSearchParams) ([]string, error) {
	if params.Name == "" && params.Ingredient == "" && len(params.Tags) == 0 {
		return getAllRecipes(ctx, db)
	}

	recipes := make([]string, 0)

	if params.Name != "" {
		r, err := getRecipesByName(ctx, db, params.Name)
		if err != nil {
			return nil, err
		}
		recipes = append(recipes, r...)
	}

	if params.Ingredient != "" {
		r, err := getRecipesByIngredient(ctx, db, params.Ingredient)
		if err != nil {
			return nil, err
		}
		recipes = append(recipes, r...)
	}

	if len(params.Tags) != 0 {
		tags := slice.Map(params.Tags, func(s string) string {
			return strings.ToLower(s)
		})
		r, err := getRecipesByTags(ctx, db, tags)
		if err != nil {
			return nil, err
		}
		recipes = append(recipes, r...)
	}

	return slice.Uniq(recipes), nil
}

func getAllRecipes(ctx context.Context, db *sql.DB) ([]string, error) {
	rows, err := db.QueryContext(ctx, `
		SELECT recipe_name
		FROM recipes
		WHERE is_parsable = 1`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	recipes := make([]string, 0)
	for rows.Next() {
		var recipe string
		if err := rows.Scan(&recipe); err != nil {
			return nil, err
		}
		recipes = append(recipes, recipe)
	}

	return recipes, nil
}

func getRecipesByName(ctx context.Context, db *sql.DB, name string) ([]string, error) {
	rows, err := db.QueryContext(ctx, `
		SELECT DISTINCT recipe_name
		FROM recipes
		WHERE is_parsable = 1
		  AND recipe_name LIKE '%'||?||'%'`, name)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	recipes := make([]string, 0)
	for rows.Next() {
		var recipe string
		if err := rows.Scan(&recipe); err != nil {
			return nil, err
		}
		recipes = append(recipes, recipe)
	}

	return recipes, nil
}

func getRecipesByIngredient(ctx context.Context, db *sql.DB, ingredient string) ([]string, error) {
	rows, err := db.QueryContext(ctx, `
		SELECT DISTINCT recipe_name
		FROM ingredients
		WHERE ingredient LIKE '%'||?||'%'`, ingredient)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	recipes := make([]string, 0)
	for rows.Next() {
		var recipe string
		if err := rows.Scan(&recipe); err != nil {
			return nil, err
		}
		recipes = append(recipes, recipe)
	}

	return recipes, nil
}

func getRecipesByTags(ctx context.Context, db *sql.DB, tags []string) ([]string, error) {
	query, args, err := sqlx.In(`
		SELECT DISTINCT recipe_name
		FROM tags
		WHERE lower(tag) IN (?)`, tags)
	if err != nil {
		return nil, err
	}
	rows, err := db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	recipes := make([]string, 0)
	for rows.Next() {
		var recipe string
		if err := rows.Scan(&recipe); err != nil {
			return nil, err
		}
		recipes = append(recipes, recipe)
	}

	return recipes, nil
}

// GetTags queries for and returns a slice of all tags
func GetTags(ctx context.Context, db *sql.DB) ([]string, error) {
	rows, err := db.QueryContext(ctx, "SELECT DISTINCT tag FROM tags")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	tags := make([]string, 0)
	for rows.Next() {
		var tag string
		if err := rows.Scan(&tag); err != nil {
			return nil, err
		}
		tags = append(tags, tag)
	}

	return tags, nil
}

// GetRecipeFileName queries for and returns the file name for a recipe
func GetRecipeFileName(ctx context.Context, db *sql.DB, recipeName string) (string, bool, error) {
	row := db.QueryRowContext(ctx, `
		SELECT file_name, is_parsable
		FROM recipes
		WHERE recipe_name = ?`, recipeName)
	var fileName string
	var isParsable uint
	err := row.Scan(&fileName, &isParsable)
	if err != nil {
		return "", false, err
	}

	return fileName, isParsable == 1, nil
}

// GetUnparsableFiles queries for and returns a slice of recipe names that
// were not parsable files
func GetUnparsableFiles(ctx context.Context, db *sql.DB) ([]string, error) {
	rows, err := db.QueryContext(ctx, `
		SELECT recipe_name
		FROM recipes
		WHERE is_parsable = 0`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	names := make([]string, 0)
	for rows.Next() {
		var name string
		if err := rows.Scan(&name); err != nil {
			return nil, err
		}
		names = append(names, name)
	}

	return names, nil
}
