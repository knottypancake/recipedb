package infra

import (
	"context"
	"database/sql"
	"log"

	"codeberg.org/knottypancake/recipedb/internal/model"
)

// InitDatabase creates the database tables
func InitDatabase(ctx context.Context, db *sql.DB) error {
	tableDefinitions := `
		DROP TABLE IF EXISTS recipes;
		CREATE TABLE recipes (
			file_name   text,
			recipe_name text,
			is_parsable integer,
			PRIMARY KEY (file_name) ON CONFLICT IGNORE,
			UNIQUE (recipe_name) ON CONFLICT IGNORE
		);
		DROP TABLE IF EXISTS ingredients;
		CREATE TABLE ingredients (
			file_name   text,
			recipe_name text,
			ingredient  text,
			PRIMARY KEY (file_name, recipe_name, ingredient) ON CONFLICT IGNORE
		);
		DROP TABLE IF EXISTS tags;
		CREATE TABLE tags (
			file_name   text,
			recipe_name text,
			tag         text,
			PRIMARY KEY (file_name, recipe_name, tag) ON CONFLICT IGNORE
		);`

	_, err := db.ExecContext(ctx, tableDefinitions)
	if err != nil {
		return err
	}

	return nil
}

// InsertRecipe upserts a recipe including its ingredients and tags
func InsertRecipe(ctx context.Context, db *sql.DB, recipe model.Recipe) error {
	log.Println("Writing recipe", recipe.RecipeName)

	parsable := 1
	if !recipe.IsParsable {
		parsable = 0
	}

	_, err := db.ExecContext(ctx, `
		INSERT INTO recipes (file_name, recipe_name, is_parsable)
		VALUES (?, ?, ?)`, recipe.FileName, recipe.RecipeName, parsable)
	if err != nil {
		return err
	}

	_, err = db.ExecContext(ctx, "DELETE FROM ingredients WHERE file_name = ?", recipe.FileName)
	if err != nil {
		return err
	}
	for _, ing := range recipe.Ingredients {
		_, err = db.ExecContext(ctx, `
			INSERT INTO ingredients (file_name, recipe_name, ingredient)
			VALUES (?, ?, ?)`, recipe.FileName, recipe.RecipeName, ing)
		if err != nil {
			return err
		}
	}

	_, err = db.ExecContext(ctx, "DELETE FROM tags WHERE file_name = ?", recipe.FileName)
	if err != nil {
		return err
	}
	for _, tag := range recipe.Tags {
		_, err = db.ExecContext(ctx, `
			INSERT INTO tags (file_name, recipe_name, tag)
			VALUES (?, ?, ?)`, recipe.FileName, recipe.RecipeName, tag)
		if err != nil {
			return err
		}
	}

	return nil
}

// RemoveRecipe removes a recipe from the index given its file name
func RemoveRecipe(ctx context.Context, db *sql.DB, fileName string) error {
	log.Println("Removing recipe", fileName)

	_, err := db.ExecContext(ctx, "DELETE FROM recipes WHERE file_name = ?", fileName)
	if err != nil {
		return err
	}

	_, err = db.ExecContext(ctx, "DELETE FROM ingredients WHERE file_name = ?", fileName)
	if err != nil {
		return err
	}

	_, err = db.ExecContext(ctx, "DELETE FROM tags WHERE file_name = ?", fileName)
	if err != nil {
		return err
	}

	return nil
}
