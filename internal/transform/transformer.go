package transform

import "github.com/russross/blackfriday/v2"

// Transform converts the markdown text into HTML
func Transform(markdown []byte) []byte {
	return blackfriday.Run(markdown)
}
