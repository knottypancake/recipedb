# Recipe DB

This is a small project that aims to fit a very specific use case:

* A database of recipes which is just a directory of Markdown (or other) files. This makes it easy for users of all levels to add and modify recipes without building a complex recipe editing UI.
* A web app that can search the recipes for the recipe name, ingredients, or tags and then display the reciped rendered as HTML. The web app should look good on phones and tablets as most people don't put laptops in the kitchen for displaying recipes.

## Installation

Currently, there are no pre-build binaries or packages; you'll need to build it yourself. You'll need `go`, `make`, `gcc` (due to the use of cgo), and the sqlite dev package. Just running `make` should build the binary for you.

There are a few helper files included which you can use if you want to manage the application with systemd and use apache as a reverse proxy; so for example, you can run the app on port 8001, but use apache to proxy `/recipedb` to `:8001`. The shell script `install-ubuntu-systemd-apache.sh` shows how you might install the helper files (as well as the asset files) on an Ubuntu system. If you do this, don't forget to run `a2enconf recipedb` to enable the apache configuration.

## TODO

* create a Docker file to make building and running easier
* write more tests
