.DEFAULT_GOAL := build

.PHONY:build
build: vet
	go build -o cmd/recipedb/recipedb cmd/recipedb/main.go

.PHONY:vet
vet: fmt
	go vet ./...

.PHONY:fmt
fmt:
	go fmt ./...

.PHONY: clean
clean:
	rm cmd/recipedb/recipedb
