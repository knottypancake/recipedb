package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"codeberg.org/knottypancake/recipedb/internal/app"
)

func main() {
	// Create a cancelable context so we can gracefully shut down if the app
	// is killed
	ctx, cancel := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer cancel()

	// Load the config
	config := app.AppConfig{
		Port:               "8001",
		AppPath:            "", // or something like /recipedb
		DatabaseDirName:    "/var/www/recipedb",
		RecipeDirName:      "/var/www/recipedb/recipes",
		StaticAssetDirName: "/var/www/recipedb/assets",
		TemplateDir:        "/var/www/recipedb/templates",
	}

	if port := os.Getenv("PORT"); port != "" {
		config.Port = port
	}

	if appPath := os.Getenv("APP_PATH"); appPath != "" {
		config.AppPath = appPath
	}

	if dbDir := os.Getenv("DATABASE_DIR"); dbDir != "" {
		config.DatabaseDirName = dbDir
	}

	if recipeDir := os.Getenv("RECIPE_DIR"); recipeDir != "" {
		config.RecipeDirName = recipeDir
	}

	if assetDir := os.Getenv("ASSET_DIR"); assetDir != "" {
		config.StaticAssetDirName = assetDir
	}

	if templateDir := os.Getenv("TEMPLATE_DIR"); templateDir != "" {
		config.TemplateDir = templateDir
	}

	// Run the application
	err := app.Run(ctx, config)
	if err != nil {
		fmt.Println(err)
	}
}
