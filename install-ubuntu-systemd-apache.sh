#!/usr/bin/bash

# This is an example of how you might install this app on an Ubuntu/systemd
# system using Apache as a reverse proxy.
# See https://httpd.apache.org/docs/2.4/howto/reverse_proxy.html

install -v -o www-data -g www-data -m 755 -d \
 /var/www/recipedb/bin \
 /var/www/recipedb/assets \
 /var/www/recipedb/assets/css \
 /var/www/recipedb/assets/img \
 /var/www/recipedb/assets/js \
 /var/www/recipedb/templates

install -v -o www-data -g www-data -m 700 cmd/recipedb/recipedb /var/www/recipedb/bin
install -v -o www-data -g www-data -m 644 web/static/img/* /var/www/recipedb/assets/img
install -v -o www-data -g www-data -m 644 web/static/css/* /var/www/recipedb/assets/css
install -v -o www-data -g www-data -m 644 web/static/js/* /var/www/recipedb/assets/js
install -v -o www-data -g www-data -m 644 web/templates/* /var/www/recipedb/templates

install -v -o root -g root -m 644 build/systemd/recipedb.service /etc/systemd/system
install -v -o root -g root -m 644 build/apache/recipedb.conf /etc/apache2/conf-available
